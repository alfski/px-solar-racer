/* Copyright 2008 Google Inc.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
// milktruck.js
// Code for Monster Milktruck demo, using Earth Plugin.
// Alf 20130312 mod MilkTruck to use GamePad API 
// Alf 20130827 mod MilkTruck to use buttons for accel and camera control

window.truck = null;

// Pull the Milktruck model from 3D Warehouse.
var PAGE_PATH = document.location.href.replace(/\/[^\/]+$/, '/');
//var MODEL_URL = 'http://px1105.scem.uws.edu.au/px/Demos/Alf-MilkTruckWheel/MilkTruckGamePad/lib/milktruck.kmz';
//var MODEL_URL = 'http://px1105.scem.uws.edu.a/working/arrow.dae';
//var MODEL_URL = 'http://sites.google.com/a/yugathian.com/files/files/milktruck.kmz';
var MODEL_URL = 'http://sites.google.com/a/yugathian.com/files/files/example.kmz';

var START_LOCS = [
	{ lat: -23.6970, lon: 133.8787, heading: 160 }, // Alice Maccas
	{ lat: -12.4655, lon: 130.8426, heading: 240 }, // Darwin
	{ lat: -34.9285, lon: 138.5999, heading: 80 }, // Adelaide 
	{ lat: -25.3456, lon: 131.0659, heading: 180 }, // Uluru
	{ lat: -33.8309, lon: 151.0204, heading: 90 }, // Parramatta
	{ lat: -23.651476, lon: 133.862435, heading: -85 }, // Alice inbound
];

var TRAIL_CAM = [
	{ tilt: 89.5, range: 3 },
	{ tilt: 0, range: 100 },
	{ tilt: 70, range: 10000 },
	{ tilt: 75, range: 500000 },
	{ tilt: 0, range: 30000000 }
];
var BCRate = [0,0,0,0,0,0.01,0.015,0.0175,0.02,0.0225,0.025,0.0275,0.03,0.0275,0.025,0.0225,0.02,0.0175,0.01,0,0,0,0,0];

var PREVENT_START_AIRBORNE = true;
var TICK_MS = 66;

var BALLOON_FG = '#000000';
var BALLOON_BG = '#FFFFFF';

var GRAVITY = 34.88;
//Alf var CAM_HEIGHT = 10;
var CAM_HEIGHT = 1.5;
//Alf var TRAILING_DISTANCE = 50;
var TRAILING_DISTANCE = 0;
var CAM_INDEX = 0; // current Camera index
var CAM_LIMIT = 4; // 0,1,2 camera
var SUNNY = true;

var ACCEL = 4.0;
var DECEL = 7.0;
var MAX_REVERSE_SPEED = 0.0;
var MAX_CRUISE_SPEED = 0.0;
var MAX_FORWARD_SPEED = 38.89;
var VIRTUAL_MAX_CRUISE = 70;

var STEER_ROLL = -1.0;
var ROLL_SPRING = 0.5;
var ROLL_DAMP = -0.16;
var sDate1 = '2013-09-26T';
var sDate2 = ':00:00+11:00';
var tTime = 00;
//Sheng's Variable==============================================
var total_distance = 0;
var instant_distance = 0;
var battery = 10;
var batteryWasEmpty = 0;
var minBatteryLevel = 10;



function Truck() {
	var me = this;

	me.doTick = true;

	// We do all our motion relative to a local coordinate frame that is
	// anchored not too far from us.  In this frame, the x axis points
	// east, the y axis points north, and the z axis points straight up
	// towards the sky.
	//
	// We periodically change the anchor point of this frame and
	// recompute the local coordinates.
	me.localAnchorLla = [0, 0, 0];
	me.localAnchorCartesian = V3.latLonAltToCartesian(me.localAnchorLla);
	me.localFrame = M33.identity();

	// Position, in local cartesian coords.
	me.pos = [0, 0, 0];

	// Velocity, in local cartesian coords.
	me.vel = [0, 0, 0];

	// Orientation matrix, transforming model-relative coords into local coords
	me.modelFrame = M33.identity();

	me.roll = 0;
	me.rollSpeed = 0;

	me.idleTimer = 0;
	me.fastTimer = 0;
	me.popupTimer = 0;

	ge.getOptions().setMouseNavigationEnabled( true );
	ge.getOptions().setFlyToSpeed(100);  // don't filter camera motion

	console.log("load " + MODEL_URL);
	window.google.earth.fetchKml(ge, MODEL_URL, function(obj) { me.finishInit(obj); });
}

Truck.prototype.finishInit = function(kml) {
	var me = this;

	walkKmlDom(kml, function() {
    if (this.getType() == 'KmlPlacemark' &&
        this.getGeometry() &&
        this.getGeometry().getType() == 'KmlModel')
		me.placemark = this;
	});

	me.model = me.placemark.getGeometry();
	me.orientation = me.model.getOrientation();
	me.location = me.model.getLocation();

	me.model.setAltitudeMode(ge.ALTITUDE_ABSOLUTE);
	me.orientation.setHeading(90);
	me.model.setOrientation(me.orientation);

	ge.getFeatures().appendChild(me.placemark);

	me.teleportTo(START_LOCS[0].lat, START_LOCS[0].lon, START_LOCS[0].heading);

	me.lastMillis = (new Date()).getTime();

	var href = window.location.href;

	// google.earth.addEventListener(ge, "frameend", function() { me.tick(); });

	me.cameraCut();

	// Make sure keyboard focus starts out on the page.
	ge.getWindow().blur();

	// If the user clicks on the Earth window, try to restore keyboard
	// focus back to the page.
	google.earth.addEventListener(ge.getWindow(), "mouseup", function(event) {
	  ge.getWindow().blur();
	});
	ge.getTime().setTimePrimitive(ge.getTime().getSystemTime()); // Set the time to current system time
	calendar.setTime(0);
}



function clamp(val, min, max) {
	if (val < min) {
		return min;
	} else if (val > max) {
		return max;
	}
	return val;
}

// Functions for time control


function change_time( amount ) // format 2002-10-10T12:00:00+11:00
{
	calendar.setTime(amount);
	// '2013-09-26T12:00:00+11:00';
}

function setOptions()
{
	ge.getSun().setVisibility(document.getElementById("show_sun").checked);
	ge.getOptions().setAtmosphereVisibility(document.getElementById("show_sun").checked);
	ge.getLayerRoot().enableLayerById(ge.LAYER_BUILDINGS, document.getElementById("show_buildings").checked);
	ge.getWindow().setVisibility(document.getElementById("show_plugin").checked);

}

// End of new functions

//====================================================================
Truck.prototype.tick = function() {
    var me = this;
    var offset = 0; // used for locations, cheating method. Should be changed.

	if (!truckLoaded) { return; }

    var now = (new Date()).getTime();
	
	set_buttons(); // Does all controller checking, updates which buttons were pressed
	setOptions();
	
    // dt is the delta-time since last tick, in seconds
    var dt = (now - me.lastMillis) / 1000.0;
    if (dt > 0.25) {
		dt = 0.25;
    }
    me.lastMillis = now;
	
	// check wheel buttons
	if (now - LastButton > 500) { // only process buttons after some time after Last Button was pressed
        for (var btn = 0; btn < 4; btn++ ) { // check buttons 2 to 7 for teleport
			if (buttons.location[btn]) {
				// console.log("teleporting to loc index " + loc);
				me.teleportTo(START_LOCS[btn].lat, START_LOCS[btn].lon, START_LOCS[btn].heading);
				LastButton = now;
			}
		}
		if (buttons.zoomOut && CAM_INDEX < CAM_LIMIT) {
			CAM_INDEX++;
			//console.log("setting trail cam to index " + CAM_INDEX );
			LastButton = now;
		} else
		if (buttons.zoomIn && CAM_INDEX > 0) {
			CAM_INDEX--;
			//console.log("setting trail cam to index " + CAM_INDEX );
			LastButton = now;
		}
	}
    
    if (now - LastButton1 > 300){
        if (buttons.accel){
            MAX_CRUISE_SPEED += (20/3.6);
            if (MAX_CRUISE_SPEED > MAX_FORWARD_SPEED){
                MAX_CRUISE_SPEED = MAX_FORWARD_SPEED;
            }
            LastButton1 = now;
        }else if (buttons.brake){
            MAX_CRUISE_SPEED -= ((20/3.6));
            if (MAX_CRUISE_SPEED < 0){
                MAX_CRUISE_SPEED = 0;
            }
            LastButton1 = now;
        }else{
		}
    }
	
	if (now - LastButton2 > 200)
	{
        if(buttons.increaseTime)   /////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			change_time (1);
			LastButton2 = now;
		}
		else if (buttons.decreaseTime)
		{			
			change_time(-1);
			LastButton2 = now; 
		}
	} 

    // holding brake
    if (buttons.brake){
        BrakeHold += dt;
    }else{
        BrakeHold = 0;
    }
    
    // hold for 1 second, cruise speed = 0
    if (BrakeHold > 0.5){
        MAX_CRUISE_SPEED = 0;
        BrakeHold = 0;
    }

  var c0 = 1;
  var c1 = 0;

  var gpos = V3.add(me.localAnchorCartesian,
                    M33.transform(me.localFrame, me.pos));
  var lla = V3.cartesianToLatLonAlt(gpos);

  if (V3.length([me.pos[0], me.pos[1], 0]) > 100) {
    // Re-anchor our local coordinate frame whenever we've strayed a
    // bit away from it.  This is necessary because the earth is not
    // flat!
    me.adjustAnchor();
  }

  var dir = me.modelFrame[1];
  var up = me.modelFrame[2];

  var absSpeed = V3.length(me.vel);

  var groundAlt = ge.getGlobe().getGroundAltitude(lla[0], lla[1]);
  var airborne = (groundAlt + 0.30 < me.pos[2]);
  var steerAngle = 0;
  
  // Steering.
    //Alf var TURN_SPEED_MIN = 60.0;  // radians/sec
    var TURN_SPEED_MIN = 20.0;  // radians/sec
    // Alf var TURN_SPEED_MAX = 100.0;  // radians/sec
    var TURN_SPEED_MAX = 30.0;  // radians/sec
 
    var turnSpeed;

    // Degrade turning at higher speeds.
    //
    //           angular turn speed vs. vehicle speed
    //    |     -------
    //    |    /       \-------
    //    |   /                 \-------
    //    |--/                           \---------------
    //    |
    //    +-----+-------------------------+-------------- speed
    //    0    SPEED_MAX_TURN           SPEED_MIN_TURN
    var SPEED_MAX_TURN = 30.0/3.6;
    var SPEED_MIN_TURN = 90.0/3.6;
    if (absSpeed < SPEED_MAX_TURN) {
      turnSpeed = TURN_SPEED_MIN + (TURN_SPEED_MAX - TURN_SPEED_MIN)
                   * (SPEED_MAX_TURN - absSpeed) / SPEED_MAX_TURN;
      turnSpeed *= (absSpeed / SPEED_MAX_TURN);  // Less turn as truck slows
    } else if (absSpeed < SPEED_MIN_TURN) {
      turnSpeed = TURN_SPEED_MIN + (TURN_SPEED_MAX - TURN_SPEED_MIN)
                  * (SPEED_MIN_TURN - absSpeed)
                  / (SPEED_MIN_TURN - SPEED_MAX_TURN);
    } else {
      turnSpeed = TURN_SPEED_MIN;
    }
	var maxTurnSpeed = 0.05;
	
  var wheelRot = buttons.turning;
	//console.log("axes[0]=" + wheelRot);

	if (Math.abs(wheelRot) > 0.035) { // center of wheel "buffer"
		steerAngle = -wheelRot/25 * turnSpeed/30;
	}
		//console.log("wheelRot="+wheelRot+" steerAngle="+steerAngle);
  
  // Turn.
  var newdir = airborne ? dir : V3.rotate(dir, up, steerAngle);
  me.modelFrame = M33.makeOrthonormalFrame(newdir, up);
  dir = me.modelFrame[1];
  up = me.modelFrame[2];

  var forwardSpeed = 0;
  
  if (!airborne && battery > minBatteryLevel || (!batteryWasEmpty && battery > 0)) {
		// TODO: if we're slipping, transfer some of the slip
		// velocity into forward velocity.

		// Damp sideways slip.  Ad-hoc frictiony hack.
		//
		// I'm using a damped exponential filter here, like:
		// val = val * c0 + val_new * (1 - c0)
		//
		// For a variable time step:
		//  c0 = exp(-dt / TIME_CONSTANT)
		var right = me.modelFrame[0];
		var slip = V3.dot(me.vel, right);
		c0 = Math.exp(-dt / 0.5);
		me.vel = V3.sub(me.vel, V3.scale(right, slip * (1 - c0)));

			//Alf - query gamepad buttons
			// Sheng - gasButtonDown = navigator.webkitGetGamepads()[0].buttons[1];
			// Sheng - reverseButtonDown = navigator.webkitGetGamepads()[0].buttons[0];
		gasButtonDown = 1;
		// Apply engine/reverse accelerations.
		forwardSpeed = V3.dot(dir, me.vel);
		  
		  //Sheng's Code==============================================
      
		if (forwardSpeed > 1){
			total_distance += forwardSpeed * dt;
      }else if (forwardSpeed < 1){
          forwardSpeed = 0;
      }else{
          
      }
      if (MAX_CRUISE_SPEED != 0){
          rACCEL = ACCEL + ACCEL * (forwardSpeed / MAX_CRUISE_SPEED);
      }else{
          rACCEL = ACCEL;
      }
      rACCEL = Math.abs(rACCEL);
      
      /*if (rACCEL > ACCEL){
          rACCEL = ACCEL;
      }else if(rACCEL < 2){
          rACCEL = 2;
       }*/
      
      // decelerate if cruise speed is decreased
      if (forwardSpeed > MAX_CRUISE_SPEED){
          gasButtonDown = 0;
          me.vel = V3.add(me.vel, V3.scale(dir, -DECEL * dt));
          if (forwardSpeed < 0){
              forwardSpeed = 0;
          }
          if (forwardSpeed > 0){
              //battery += 5;
          }
      }
      if (gasButtonDown) {
          // Accelerate forwards.
          if (forwardSpeed < MAX_CRUISE_SPEED) {
              me.vel = V3.add(me.vel, V3.scale(dir, rACCEL * dt));
          }
      } /*else if (reverseButtonDown) {
          if (forwardSpeed > -MAX_REVERSE_SPEED){
              me.vel = V3.add(me.vel, V3.scale(dir, -DECEL * dt));
          }
      }*/
		
  }

  // Air drag.
  //
  // Fd = 1/2 * rho * v^2 * Cd * A.
  // rho ~= 1.2 (typical conditions)
  // Cd * A = 3 m^2 ("drag area")
  //
  // I'm simplifying to:
  //
  // accel due to drag = 1/Mass * Fd
  // with Milktruck mass ~= 2000 kg
  // so:
  // accel = 0.6 / 2000 * 3 * v^2
  // accel = 0.0009 * v^2
  absSpeed = V3.length(me.vel);
  if (absSpeed > 0.0001) {
    var veldir = V3.normalize(me.vel);
    var DRAG_FACTOR = 0.00090;
    var drag = absSpeed * absSpeed * DRAG_FACTOR;

    // Some extra constant drag (rolling resistance etc) to make sure
    // we eventually come to a stop.
    var CONSTANT_DRAG = 3.0;
    drag += CONSTANT_DRAG;

    if (drag > absSpeed) {
      drag = absSpeed;
    }

    me.vel = V3.sub(me.vel, V3.scale(veldir, drag * dt));
  }

  // Gravity
  me.vel[2] -= GRAVITY * dt;

  // Move.
  var deltaPos = V3.scale(me.vel, dt);
  me.pos = V3.add(me.pos, deltaPos);

  gpos = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, me.pos));
  lla = V3.cartesianToLatLonAlt(gpos);
  
  // Don't go underground.
  groundAlt = ge.getGlobe().getGroundAltitude(lla[0], lla[1]);
  if (me.pos[2] < groundAlt) {
    me.pos[2] = groundAlt;
  }

  var normal = estimateGroundNormal(gpos, me.localFrame);
  
  if (!airborne) {
    // Cancel velocity into the ground.
    //
    // TODO: would be fun to add a springy suspension here so
    // the truck bobs & bounces a little.
    var speedOutOfGround = V3.dot(normal, me.vel);
    if (speedOutOfGround < 0) {
        me.vel = V3.add(me.vel, V3.scale(normal, -speedOutOfGround));
    }

    // Make our orientation follow the ground.
    c0 = Math.exp(-dt / 0.25);
    c1 = 1 - c0;
    var blendedUp = V3.normalize(V3.add(V3.scale(up, c0),
                                        V3.scale(normal, c1)));
    me.modelFrame = M33.makeOrthonormalFrame(dir, blendedUp);
  }

  // Propagate our state into Earth.
  gpos = V3.add(me.localAnchorCartesian,
                M33.transform(me.localFrame, me.pos));
  lla = V3.cartesianToLatLonAlt(gpos);
  //Alf me.model.getLocation().setLatLngAlt(lla[0], lla[1], lla[2]);

  var newhtr = M33.localOrientationMatrixToHeadingTiltRoll(me.modelFrame);

  // Compute roll according to steering.
  // TODO: this would be even more cool in 3d.
  var absRoll = newhtr[2];
  me.rollSpeed += steerAngle * forwardSpeed * STEER_ROLL;
  // Spring back to center, with damping.
  me.rollSpeed += (ROLL_SPRING * -me.roll + ROLL_DAMP * me.rollSpeed);
  me.roll += me.rollSpeed * dt;
  me.roll = clamp(me.roll, -30, 30);
  absRoll += me.roll;

  me.orientation.set(newhtr[0], newhtr[1], absRoll);

    me.cameraFollow(dt, gpos, me.localFrame);
    var batteryRate = 0;
    //=============battery
    
	if (document.getElementById("infinite_battery").checked == false)
	{
        //battery charge rate changes through the day.
        batteryRate = BCRate[(calendar.hour % 24)];
        //batteryRate = calendar.hour%24;
		if (forwardSpeed > 0){
			battery -= forwardSpeed/700;
		}
		if (forwardSpeed > 27.77){
			battery -= forwardSpeed/300;
		}
		if (battery < 0){
			battery = 0;
		}
		if (battery <= 0)
			batteryWasEmpty = 1;
		if (SUNNY){
			battery += batteryRate;
		}
		
		if (battery > minBatteryLevel)
			batteryWasEmpty = 0;
		if (battery > 100){
			battery = 100;
		}
	}
	else
	{
		battery = 100;
	}
    
    //Sheng's code again ==========================================
    var n = total_distance / 1000;
    var k = n.toFixed(3);
    
    //=========================
    var forwardSpeed_km = absSpeed * 3.6;
    var f = forwardSpeed_km.toFixed(2);
    
    //=========================
    var cmpass = Math.atan(dir[0]/dir[1])/Math.PI * 180;
    //cmpass = Math.abs(cmpass);
    if (dir[0] >= 0 && dir[1] >= 0){
        cmpass = cmpass;
    }else if (dir[0] >= 0 && dir[1] < 0){
        cmpass = cmpass + 180;
    }else if (dir[0] <= 0 && dir[1] <= 0){
        cmpass = cmpass + 180;
    }else if (dir[0] <= 0 && dir[1] > 0){
        cmpass = cmpass + 360;
    }
    cmpass = cmpass.toFixed(2);
    
    //=========================
    var tilt = dir[2];
    
    if (tilt >=0){
        tilt *= 90;
    } else if(tilt < 0){
        tilt *= 90;
    }
    tilt = tilt.toFixed(2);
    //=========================
    
    var d = Math.floor (lla[0]);
    var minfloat = (lla[0]-d)*60;
    var m = Math.floor(minfloat);
    var secfloat = (minfloat-m)*60;
    var s = Math.round(secfloat);
    // After rounding, the seconds might become 60. These two
    // if-tests are not necessary if no rounding is done.
    if (s==60) {
        m++;
        s=0;
    }
    if (m==60) {
        d++;
        m=0;
    }
    var lla0 = "" + d + "°" + m + "\"" + s + "\'";
    
    var d1 = Math.floor (lla[1]);
    var minfloat1 = (lla[1]-d1)*60;
    var m1 = Math.floor(minfloat1);
    var secfloat1 = (minfloat1-m1)*60;
    var s1 = Math.round(secfloat1);
    // After rounding, the seconds might become 60. These two
    // if-tests are not necessary if no rounding is done.
    if (s1==60) {
        m1++;
        s1=0;
    }
    if (m1==60) {
        d1++;
        m1=0;
    }
    var lla1 = "" + d1 + "°" + m1 + "\"" + s1 + "\'";
    
    //Displaying=============================
	
	localStorage.k = k;;
    
    localStorage.f = f;
    
    localStorage.cmpass = cmpass;
    
	localStorage.roll = me.roll.toFixed(2);
    
    localStorage.tilt = tilt;
    localStorage.maxspeed = (MAX_CRUISE_SPEED * 3.6).toFixed(2);
	localStorage.lat = lla0;
	localStorage.Long = lla1;
    localStorage.battery = battery.toFixed(2);
	
	// Display on this screen
	
    document.getElementById("distanceKMs").innerHTML = k + " Km";
    
    document.getElementById("vehicleSpeed").innerHTML = (f + " Km/h");
    
    document.getElementById("compass").innerHTML = cmpass + " degree";
    
	document.getElementById("vehicleRoll").innerHTML = me.roll.toFixed(2);
	//document.getElementById("vehicleRoll").innerHTML = batteryRate;
    
    document.getElementById("vehicleTilt").innerHTML = tilt;
    document.getElementById("maxSpeed").innerHTML = (MAX_CRUISE_SPEED * 3.6).toFixed(2) + " Km/h";
	document.getElementById("latLong").innerHTML = (lla0 + " / " + lla1);
    document.getElementById("battery").innerHTML = battery.toFixed(2) + '(' + BCRate[calendar.hour.mod(24)] + ')';
	document.getElementById("time").innerHTML = (calendar.hour.mod(12) == 0 ? '12' : calendar.hour.mod(12)) + ':' + calendar.minute + ':' + calendar.second + ((calendar.hour.mod(24) < 12) ? 'am' : 'pm');
	
    
    
};// tick end
//=====================================================================

// TODO: would be nice to have globe.getGroundNormal() in the API.
function estimateGroundNormal(pos, frame) {
  // Take four height samples around the given position, and use it to
  // estimate the ground normal at that position.
  //  (North)
  //     0
  //     *
  //  2* + *3
  //     *
  //     1
  var pos0 = V3.add(pos, frame[0]);
  var pos1 = V3.sub(pos, frame[0]);
  var pos2 = V3.add(pos, frame[1]);
  var pos3 = V3.sub(pos, frame[1]);
  var globe = ge.getGlobe();
  function getAlt(p) {
    var lla = V3.cartesianToLatLonAlt(p);
    return globe.getGroundAltitude(lla[0], lla[1]);
  }
  var dx = getAlt(pos1) - getAlt(pos0);
  var dy = getAlt(pos3) - getAlt(pos2);
  var normal = V3.normalize([dx, dy, 2]);
  return normal;
}

Truck.prototype.startTruck = function() {
  var me = this;
  google.earth.addEventListener(ge, "frameend", function() { me.tick(); });
};

Truck.prototype.stopTruck = function() {
  var me = this;
  //google.earth.removeEventListener(ge, "frameend", function() { me.tick(); });
  google.earth.removeEventListener(ge, "frameend", false );
};

// Cut the camera to look at me.
Truck.prototype.cameraCut = function() {
	var me = this;
	var lo = me.model.getLocation();
	var la = ge.createLookAt('');
	la.set(lo.getLatitude(), lo.getLongitude(),
		// Alf 10 /* altitude */,
		4 /* altitude */,
		ge.ALTITUDE_RELATIVE_TO_SEA_FLOOR,
		fixAngle(180 + me.model.getOrientation().getHeading() + 45),
		80, /* tilt */
		0 /* range */          // Alf was 50
		);
	ge.getView().setAbstractView(la);
};

Truck.prototype.cameraFollow = function(dt, truckPos, localToGlobalFrame) {
	var me = this;

	var c0 = Math.exp(-dt / 0.5);
	var c1 = 1 - c0;

	var la = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_SEA_FLOOR);

	var truckHeading = me.model.getOrientation().getHeading();
	var camHeading = la.getHeading();

	var deltaHeading = fixAngle(truckHeading - camHeading);
	var heading = camHeading + c1 * deltaHeading;
	heading = fixAngle(heading);
	//console.log(heading);

	var headingRadians = heading / 180 * Math.PI;

	var headingDir = V3.rotate(localToGlobalFrame[1], localToGlobalFrame[2],
							 -headingRadians);
	var camPos = V3.add(truckPos, V3.scale(localToGlobalFrame[2], CAM_HEIGHT));
	camPos = V3.add(camPos, V3.scale(headingDir, -TRAILING_DISTANCE));
	var camLla = V3.cartesianToLatLonAlt(camPos);
	var camLat = camLla[0];
	var camLon = camLla[1];
	var camAlt = camLla[2] - ge.getGlobe().getGroundAltitude(camLat, camLon);
	
	localStorage.latFull = camLat;
	localStorage.longFull = camLon;
	
	//la.set(camLat, camLon, camAlt, ge.ALTITUDE_RELATIVE_TO_SEA_FLOOR, 
	//      heading, 80 /*tilt*/, 0 /*range*/);
	la.set(camLat, camLon, camAlt, ge.ALTITUDE_RELATIVE_TO_SEA_FLOOR, 
		heading, (TRAIL_CAM[CAM_INDEX].tilt + me.modelFrame[1][2] * 100).mod(360) /*tilt*/, TRAIL_CAM[CAM_INDEX].range /*range*/);
	
	ge.getView().setAbstractView(la);
};

// heading is optional.
Truck.prototype.teleportTo = function(lat, lon, heading) {
	var me = this;
	me.model.getLocation().setLatitude(lat);
	me.model.getLocation().setLongitude(lon);
	me.model.getLocation().setAltitude(ge.getGlobe().getGroundAltitude(lat, lon));
	if (heading == null) {
		heading = 0;
	}
	me.vel = [0, 0, 0];

	me.localAnchorLla = [lat, lon, 0];
	me.localAnchorCartesian = V3.latLonAltToCartesian(me.localAnchorLla);
	me.localFrame = M33.makeLocalToGlobalFrame(me.localAnchorLla);
	me.modelFrame = M33.identity();
	me.modelFrame[0] = V3.rotate(me.modelFrame[0], me.modelFrame[2], -heading);
	me.modelFrame[1] = V3.rotate(me.modelFrame[1], me.modelFrame[2], -heading);
	me.pos = [0, 0, ge.getGlobe().getGroundAltitude(lat, lon)];

	// me.cameraCut();

	// make sure to not start airborne
	if (PREVENT_START_AIRBORNE) {
	window.setTimeout(function() {
		var groundAlt = ge.getGlobe().getGroundAltitude(lat, lon);
		var airborne = (groundAlt + 0.30 < me.pos[2]);
		if (airborne)
			me.teleportTo(lat, lon, heading);
		}, 500);
	}
};

// Move our anchor closer to our current position.  Retain our global
// motion state (position, orientation, velocity).
Truck.prototype.adjustAnchor = function() {
	var me = this;
	var oldLocalFrame = me.localFrame;

	var globalPos = V3.add(me.localAnchorCartesian, M33.transform(oldLocalFrame, me.pos));
	var newAnchorLla = V3.cartesianToLatLonAlt(globalPos);
	newAnchorLla[2] = 0;  // For convenience, anchor always has 0 altitude.

	var newAnchorCartesian = V3.latLonAltToCartesian(newAnchorLla);
	var newLocalFrame = M33.makeLocalToGlobalFrame(newAnchorLla);

	var oldFrameToNewFrame = M33.transpose(newLocalFrame);
	oldFrameToNewFrame = M33.multiply(oldFrameToNewFrame, oldLocalFrame);

	var newVelocity = M33.transform(oldFrameToNewFrame, me.vel);
	var newModelFrame = M33.multiply(oldFrameToNewFrame, me.modelFrame);
	var newPosition = M33.transformByTranspose(
		newLocalFrame,
		V3.sub(globalPos, newAnchorCartesian));

	me.localAnchorLla = newAnchorLla;
	me.localAnchorCartesian = newAnchorCartesian;
	me.localFrame = newLocalFrame;
	me.modelFrame = newModelFrame;
	me.pos = newPosition;
	me.vel = newVelocity;
}

function fixAngle(a) { // Keep an angle in [-180,180]
	while (a < -180) { a += 360; }
	while (a > 180) { a -= 360; }
	return a;
}
