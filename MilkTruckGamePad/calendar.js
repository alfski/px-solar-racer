Number.prototype.mod = function(n) {
return ((this%n)+n)%n;
}

var calendar = { // Controls scheme for multiple gamepads. 
		'second': 0,
		'minute': 0,
		'hour': 0,
		'day': 0,
		'month': 0,
		'year': 0,
		'timeZone': "11:00",
		'monthMaxDay' : function()
		{
			switch (this.month)
			{
				case 9:
				case 4:
				case 6:
				case 11:
					return 30;
					break;
				case 2:
					if (this.year % 4 == 0 & this.year % 400 != 0)
						return 29;
					else
						return 28;
					break;
				default:
					return 31;
					break;
			}
		},
		'setTime': function(amount) // Xbox Controller
		{
			
			try
			{
				var time = ge.getTime().getTimePrimitive().getWhen().get();
			}
			catch(err)
			{
				ge.getTime().setTimePrimitive(ge.getTime().getSystemTime()); // Set the time to current system time
			}
			
			var temp1 = time.split("T"); // SPlits into date + time
			var date = temp1[0].split("-");
			temp2 = temp1[1].substr(0,8);
			var temp3 = temp2.split(":"); // Splits time into minutes, seconds, hours
			
			this.hour = parseInt(temp3[0]);
			this.minute = parseInt(temp3[1]);
			this.second = parseInt(temp3[2]);
			
			this.year = parseInt(date[0]);
			this.month = parseInt(date[1]);
			this.day = parseInt(date[2]);
			
			// Change time by One Hour
			
			this.hour = this.hour + amount + parseInt(this.timeZone);

			if (this.hour + this.timeZone > 23 || this.hour + this.timeZone < 0)
			{
				this.hour = this.hour.mod(24);
				this.day += amount;
				if (this.day < 1 || this.day > this.monthMaxDay())
				{
					this.month += amount;
					if (amount == 1)
						this.day = 1;
					else
						this.day = this.monthMaxDay();
					if (this.month < 1 || this.month > 12)
					{
						this.month = ((this.month - 1).mod(12)) + 1;
						this.year += amount;
					}
				}
			}
			
			// Assume values are correct, merge them back into a string to send to ge plugin
			
			time = this.year + "-" + (this.month < 10 ? "0" + this.month : this.month) + "-" + (this.day < 10 ? ("0" + this.day) : this.day);
			time = time + "T" + (this.hour < 10 ? "0" + this.hour : this.hour) + ":" + (this.minute < 10 ? "0" + this.minute : this.minute) + ":" + (this.day < 10 ? "0" + this.day : this.day) + "+" + this.timeZone;
			
			var gts = ge.createTimeStamp('');
			gts.getWhen().set(time);
			ge.getTime().setTimePrimitive(gts); 
		}
	};