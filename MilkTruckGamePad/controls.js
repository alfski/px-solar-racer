// Controls.js - does all the controller checking and provides a handy interaction for tour.js

var controls = { // Controls scheme for multiple gamepads. 
	'forward': 0,
	'brake': 0,
	'left': 0,
	'right': 0,
	'zoom_in': 0,
	'zoom_out': 0,
	'increase_time': 0,
	'decrease_time': 0,
	'location' : [0,0,0,0],
	'set_gamepad': function() // Xbox Controller
    {
        this.forward = 5;
        this.brake = 4;
        this.left = 0;
        this.right = 0;
        this.zoom_in = 12;
        this.zoom_out = 13;
		this.increase_time = 9;
		this.decrease_time = 8;
		this.location[0] = 0;
		this.location[1] = 1;
		this.location[2] = 2;
		this.location[3] = 3;
    },
	'set_wheel': function() // confirm these allocations
    {
        this.forward = 1;
        this.brake = 0;
        this.left = 0;
        this.right = 0;
        this.zoom_in = 9;
        this.zoom_out = 8;
		this.increase_time = 2;
		this.decrease_time = 3;
		this.location[0] = 4;
		this.location[1] = 5;
		this.location[2] = 6;
		this.location[3] = 7;
    }
};

var buttons = { // Check for the tour.js file to look at.
	'accel' : 0,
	'brake' : 0,
	'turning' : 0.0,
	'zoomIn' : 0,
	'zoomOut' : 0,
	'increaseTime' : 0,
	'decreaseTime' : 0,
	'location' : [0,0,0,0],
	'reset' : function ()
	{
		this.accel = 0;
		this.brake = 0;
		this.left = 0;
		this.right = 0;
		this.zoomIn = 0;
		this.zoomOut = 0;
		this.increaseTime = 0;
		this.decreaseTime = 0;
		this.location[0] = 0;
		this.location[1] = 0;
		this.location[2] = 0;
		this.location[3] = 0;
	}
};
	
var LastButton = 0; // time of last button.
var LastButton1 = 0;
var LastButton2 = 0;

var gasButtonDown = false;
var reverseButtonDown = false;

function check_controller( )
{
	var temp;
	if (document.getElementById("gamepad").checked)
		temp = 'gamepad';
	else if (document.getElementById("wheel").checked)
		temp = 'wheel';
	else if ((document.getElementById("keyboard").checked))
		temp = 'keyboard';
	if (temp != gamepad)
	{
		if (temp == 'gamepad')
			controls.set_gamepad();
		else if (temp == 'wheel')
			controls.set_wheel();
		else if (temp == 'keyboard')
			controls.set_keyboard();
		gamepad = temp;
	}
}

function set_buttons( )
{
	check_controller ( );
	
	buttons.reset();
	
	if (gamepad == 'gamepad' || gamepad == 'wheel')
	{
		// acceleration and braking
		if (navigator.webkitGetGamepads()[0].buttons[controls.forward])
			buttons.accel = 1;
		else if (navigator.webkitGetGamepads()[0].buttons[controls.brake])
			buttons.brake = 1;
		
		// left and right
		
		//locations
		for (var btn = 0; btn < 4; btn++ ) // check buttons for locator
			if (navigator.webkitGetGamepads()[0].buttons[controls.location[btn]]) 
			{
				buttons.location[0] = buttons.location[1] = buttons.location[2] = buttons.location[3] = 0;
				buttons.location[btn] = 1;
			}
		
		// zooming
		if (navigator.webkitGetGamepads()[0].buttons[controls.zoom_out])
			buttons.zoomOut = 1;
		else if (navigator.webkitGetGamepads()[0].buttons[controls.zoom_in])
			buttons.zoomIn = 1;
		
		// time manipulation
		if (navigator.webkitGetGamepads()[0].buttons[controls.increase_time])
			buttons.increaseTime = 1;
		else if (navigator.webkitGetGamepads()[0].buttons[controls.decrease_time])
			buttons.decreaseTime = 1;
			
		buttons.turning = navigator.webkitGetGamepads()[0].axes[0];
	}
}
